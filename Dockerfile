# Builder stage
FROM microsoft/dotnet:2.2-sdk AS builder-stage

# Set working directory to /app
WORKDIR /app

# Copy project from the source to the docker container filesystem
COPY DemoWebApi/*.csproj ./DemoWebApi/

# Build the application inside our docker container
RUN dotnet restore DemoWebApi
COPY ./ ./

# Publish our application
RUN dotnet publish DemoWebApi/DemoWebApi.csproj -c Release -o /app/out

# Server listens on port 80
EXPOSE 80

# Runtime stage
FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime-stage

WORKDIR /app

# Copy the published image from the builder-stage above, inside a final image
COPY --from=builder-stage /app/out .

# Entry point telling docker what file to start for our application
ENTRYPOINT [ "dotnet" ] 
CMD [ "DemoWebApi.dll" ]